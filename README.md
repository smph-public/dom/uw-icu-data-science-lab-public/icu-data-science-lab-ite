# ICU Data Science Lab ITE

**Description**  
This repository provides template code pairing with numerous publications focusing on Individualized treatment effect (ITE) prediction tasks. Below is a list of the folders and the corresponding publication. Please see the READ_ME file in each folder for detailed information. 

**Contents List**  
* BOUGIE  
Seitz et al. Individualized Treatment Effects of Bougie vs Stylet for Tracheal Intubation in Critical Illness. Am J Respir Crit Care Med. 2023 Mar 6. doi: 10.1164/rccm.202209-1799OC.  
    https://pubmed.ncbi.nlm.nih.gov/36877594/  

* PILOT_vs_ICU_ROX  
Buell KG, Spicer AB, Casey JD, et al. Individualized Treatment Effects of Oxygen Targets in Mechanically Ventilated Critically Ill Adults. JAMA. 2024;331(14):1195–1204. doi:10.1001/jama.2024.2933  
    https://jamanetwork.com/journals/jama/fullarticle/2816677  

* XBOT
Afshar M, Graham Linck EJ, Spicer AB, Rotrosen J, Salisbury-Afshar EM, Sinha P, Semler MW, Churpek MM. Machine Learning-Driven Analysis of Individualized Treatment Effects Comparing Buprenorphine and Naltrexone in Opioid Use Disorder Relapse Prevention. J Addict Med. 2024 May 22. doi: 10.1097/ADM.0000000000001313. Epub ahead of print. PMID: 38776423.  
https://pubmed.ncbi.nlm.nih.gov/38776423/


**Usage**  
To use this code, follow these steps:  
1.) Clone or download the repository to your local machine.  
2.) Install the required dependencies.  
3.) Open the R script and modify the data loading and preprocessing code to fit your data.  
4.) Run the code to fit the ITE models, make predictions, and assess/illustrate model performance in the test dataset. 