import random 
import pandas as pd
import numpy as np
#https://github.com/tonyduan/hte-prediction-rcts/blob/master/src/evaluate.py
#CF in Diabetes Accord Study
def c_statistic(pred_rr, y, w):
    """
    Return concordance-for-benefit, the proportion of all matched pairs with
    unequal observed benefit, in which the patient pair receiving greater
    treatment benefit was predicted to do so.
    """
    # ensure results are reproducible
    random.seed(123)
    assert len(pred_rr) == len(w) == len(y)

    # match all pairs on predicted benefit
    tuples = list(zip(pred_rr, y, w))
    untreated = list(filter(lambda t: t[2] == 0, tuples))
    treated = list(filter(lambda t: t[2] == 1, tuples))

    # randomly subsample to ensure every person is matched
    if len(treated) < len(untreated):
        untreated = random.sample(untreated, len(treated))
    if len(untreated) < len(treated):
        treated = random.sample(treated, len(untreated))
    assert len(untreated) == len(treated)
    untreated = sorted(untreated, key=lambda t: t[0])
    treated = sorted(treated, key=lambda t: t[0])
    obs_benefit_dict = {
        (0, 0): 0,
        (0, 1): -1,
        (1, 0): 1,
        (1, 1): 0,
    }
    # calculate observed and predicted benefit for each pair
    pairs = list(zip(untreated, treated))
    obs_benefit = [obs_benefit_dict[(t[1], u[1])] for (u, t) in pairs]
    pred_benefit = [np.mean([t[0], u[0]]) for (u, t) in pairs]

    # iterate through all (N choose 2) pairs
    count, total = 0, 0
    for i in range(len(pairs)):
        for j in range(i + 1, len(pairs)):
            if obs_benefit[i] != obs_benefit[j]:
                if (obs_benefit[i] < obs_benefit[j] and
                            pred_benefit[i] < pred_benefit[j]) or \
                    (obs_benefit[i] > obs_benefit[j] and
                             pred_benefit[i] > pred_benefit[j]):
                    count += 1
                total += 1
    #print(count)
    #print(total)
    return count/total

