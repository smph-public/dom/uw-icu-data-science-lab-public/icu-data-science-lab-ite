# XBOT
## Goal  
The aim of this project was to build an Individualized Treatment Effect (ITE) prediction model for the effect of sublingual buprenorphine-naloxone vs extended release naltrexone on risk of relapse. We built the model in one random half of the X-BOT randomized clinical trial (RCT) and validated the model in the remaining half. We compared several different models for predicting ITE (also known as conditional average treatment effect, or CATE). Our analysis demonstrates that
1. Several ITE models can identify treatment effect heterogeneity in a single trial. 
2. Use of ITE models could help inform personalized treatment decisions for patients seeking medication for opioid use disorder. 

## File descriptions
Here is the order the files should be used in. Each file is dependent on successful completion of the previous file.  
- TrialCleaning.R -> text description of data cleaning process and required format  
- RunITEModels.R -> code for imputing missing data, building the Tian-EN, Uplift-RF and X-BART models, and making predictions
- Analysis.R -> code for breaking the test data into quantiles, visualizing patient characteristics by quantiles, calculating discrimination metrics (Qini, C-for-Benefit, and LRT p-value), and variable importance  
    - calls c_stat_from_pythion_to_source.py 
- varkey.csv -> example file indicating which variables of data.csv are of interest as covariates (coVars), categorical, and baseline. Further description available in TrialCleaning.R. For illustration purposes only. 
- data.csv -> example data file. For illustration purposes only. 
- causaltoolboxModifications.R -> text description of minor modifications to causaltoolbox package in order to obtain variable importance estimates for X-BART model and to allow user to set seed for underlying mc.wbart calls. 
- folder containing causaltoolbox modified files

## Dependencies
R version 3.6.3  
caret_6.0.86 -> for missing data imputation  
dplyr_1.0.2 -> for cleaner/easier data munipulation  
tableone_0.12.0 -> for summarizing by quantile  
tools4uplift_1.0.0 -> for calculating Qini  
reticulate_1.32.0 -> for python code for C-for-Benefit  
uplift 0.3.5 -> for uplift-rf method and tian_transf function
causaltoolbox 0.0.2.000-> for x_bart method

## Publication
[Afshar M, Graham Linck EJ, Spicer A., Rotrosen J, Salisbury-Afshar E, Sinha P, Semler MW, Churpek MC. Machine Learning-Driven Analysis of Individualized Treatment Effects Comparing Buprenorphine and Naltrexone in Opioid Use Disorder Relapse Prevention. DOI: 10.1097/ADM.0000000000001313.](https://pubmed.ncbi.nlm.nih.gov/38776423/)

## Method references
[Tian transformation with elastic net: Tian L, Alizadeh AA, Gentles AJ, Tibshirani A. A Simple Method for Estimating Interactions  between a Treatment and a Large Number of Covariates. J Am Stat Assoc. 2014 Oct; 109(508): 1517-1532. PMID: 25729117.](https://pubmed.ncbi.nlm.nih.gov/25729117/)

[Uplift-RF: Leo Guelman NG, Perez-Marin AM. Random forests for uplift modeling: An insurance customer retention case. Modeling and Simulation in Engineering, Economics and Management. Spring, 2012(115): 122-133.](https://www.semanticscholar.org/paper/Uplift-Random-Forests-Guelman-Guill%C3%A9n/d7e46f61b5270f643805e77487d96a518e423708)

[X-BART: Kunzel SR, Skehom JS, Bickel PJ and Yu B. Metalearners for estimating heterogeneous treatment effects using machine learning. Proc Natl Acad Sci USA. 2019; 116: 4156-4165](https://www.pnas.org/doi/full/10.1073/pnas.1804597116)
