# PILOT_vs_ICU-ROX
## Goal  
The aim of this project was to build an Individualized Treatment Effect (ITE) prediction model in one randomized clinical trial (RCT) and validate in a separate RCT for two temporally and geographically distinct patient cohorts. Our analysis....
1. demonstrates proof of concept that predicted individualized treatment effects could inform, improve, and individualize the clinical care of patients  
2. shows how oxygenation targets that are individualized using machine-learning analyses of randomized trials may reduce mortality for critically ill adults


## File descriptions
Here is the order the files should be used in. Each file is dependent on successful completion of the previous file.  
- TrialCleaning.R -> text description of data cleaning process and required format  
- RunITEModels.R -> code for imputing missing data, building the Rboost models, and making predictions
- Analysis.R -> code for breaking the test data into quantiles, visualizing patient characteristics by quantiles, calculating discrimination metrics (Adj Qini, C-for-Benefit, and LRT p-value), and variable importance  
    - calls c_stat_from_pythion_to_source.py 

## Dependencies
R version 3.6.3  
caret_6.0.86 -> for missing data imputation  
dplyr_1.0.2 -> for cleaner/easier data munipulation  
tableone_0.12.0 -> for summarizing by quantile  
tools4uplift_1.0.0 -> for calculating Adjusted Qini  
reticulate_1.32.0 -> for python code for C-for-Benefit  
rlearner -> https://github.com/xnie/rlearner/tree/master

## Reference
The theory behind and code for the Rboost model used in this analysis is based on the following paper:
X Nie, S Wager, Quasi-oracle estimation of heterogeneous treatment effects, Biometrika, Volume 108, Issue 2, June 2021, Pages 299–319, https://doi.org/10.1093/biomet/asaa076  
Please cite this paper if you use this code in your research.
